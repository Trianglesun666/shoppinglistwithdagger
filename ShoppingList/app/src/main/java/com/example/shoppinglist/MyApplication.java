package com.example.shoppinglist;

import android.app.Application;

public class MyApplication extends Application {//Экземпляр компонента..............................................................................
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent =
                DaggerAppComponent
                        .builder()
                        .appModule(new AppModule())
                        .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
