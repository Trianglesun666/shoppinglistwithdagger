package com.example.shoppinglist.data;

public class Storgate {
    //класс модель для данных из файла storgate.txt

    private String name;

    public Storgate(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
