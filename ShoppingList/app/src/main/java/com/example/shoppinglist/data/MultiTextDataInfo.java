package com.example.shoppinglist.data;


public class MultiTextDataInfo {
        //хранит данные для автодополнения
    public static String items[] = {
            "йогурт", "кетчуп", "кефир",
            "колбаса", "консервы","кофе",
            "рис", "гречка", "кукуруза", "пшено", "манка", "перловка",
            "майонез", "макароны", "масло", "молоко", "свинина", "говядина", "курятина", "субпродукты", "фарш",
            "картошка", "морковь", "лук", "свекла", "капуста", "помидоры", "огурцы",
            "раба свежая", "рыба мороженая", "рыба копченая",
            "шоколад", "пирожные", "печенье", "тортики", "сметана",
            "соки", "тар-тар", "соевый", "соус",
            "специи", "сыр", "брынза", "фрукты", "хлеб", "чай", "яйца", "свитер", "велосипед"};
}
