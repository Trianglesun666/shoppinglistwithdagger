package com.example.shoppinglist;

import android.app.Activity;
import android.app.Application;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.shoppinglist.adapters.BackgroundContainer;
import com.example.shoppinglist.adapters.StableArrayAdapter;
import com.example.shoppinglist.data.MultiTextDataInfo;
import com.example.shoppinglist.data.Storgate;
import com.example.shoppinglist.detectors.SwipeDetector;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.Component;
import dagger.Module;
import dagger.Provides;

/*import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;*/


class ListStorgate {
    ArrayList<Storgate> storages = new ArrayList<Storgate>();

    public ArrayList<Storgate> getStorage() {
        return storages;
    }

}

@Module
class AppModule {//Модуль - класс описывающий дагеру создание объектов....................................................................................................
    @Provides
    public ListStorgate stor() {
        return new ListStorgate();
    }

}

@Component(modules = {AppModule.class})//Компонент - структура, связывающая модуль, и класс - потребитель объектов модуля..................................................
interface AppComponent {
    void inject(MainActivity activity);
}

/*@ContentView(R.layout.activity_main)*/
public class MainActivity extends Activity {
    AppComponent getAppComponent() {
        return ((MyApplication)getApplication()).getAppComponent();//Метод, дающий доступ к компоненту из активити........................................................
    }
    StableArrayAdapter mAdapter;
    //Инджектим лейауты
    /*@InjectView(R.id.listView)
    private ListView mListView;
    @InjectView(R.id.autoCompleteTextView)
    private AutoCompleteTextView mAutoCompleteTextView;
    @InjectView(R.id.listViewBackground)
    private BackgroundContainer mBackgroundContainer;
    @InjectView(R.id.textViewCount)
    private TextView textViewCount;


    //Инжектим ресурсы
    @InjectResource(R.string.FILENAME)
    private String FILENAME;*/

    //Так-же можем присоединить системные сервисы, например: "PowerManager".

    final String FILENAME = "storgate.txt"; //адрес файла
    ArrayList<String> mShopItem = new ArrayList<String>(); //список из элементов в листе(не сами элемнеты)
    ArrayList<String> item= new ArrayList<String>();
    ArrayList<Storgate> storgates = new ArrayList<>(); //список в который загружаются элементы из листа
    //Swipe example

    //StableArrayAdapter mAdapter; //адаптер для свайпов

    ListView mListView;  //лист с элементами
    public AutoCompleteTextView mAutoCompleteTextView; //ссылка
    BackgroundContainer mBackgroundContainer; //подложка для итема, не используется
    TextView textViewCount; //количество элементов

    boolean mSwiping = false; //зарегестрирован сваип или нет
    boolean mItemPressed = false; //нажат или нет
    HashMap<Long, Integer> mItemIdTopMap = new HashMap<Long, Integer>(); //мап из адресов и ид
    //end swipe example
    private static final int SWIPE_DURATION = 150; //длина свайпа
    private static final int MOVE_DURATION = 90; //длительность перетаскивания
    private static AppComponent appComponent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);//Вызываем метод компонента.............................................................................................................
        //Установили лейаут для активити, здесь убрали
        setContentView(R.layout.activity_main); //устанавливаем контроллер(этот класс) лайауту
        for (String str: MultiTextDataInfo.items) { //
            item.add(str); //получаем элементы для автодополнения текста из
        }
        readFile(); //читаем файл с сохраненными данными


        mListView = (ListView) findViewById(R.id.listView); // связываем компоненты из лайаут и экземпляры
        mAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView); // связываем компоненты из лайаут и экземпляры
        mBackgroundContainer = (BackgroundContainer) findViewById(R.id.listViewBackground); // связываем компоненты из лайаут и экземпляры
        textViewCount = (TextView) findViewById(R.id.textViewCount); // связываем компоненты из лайаут и экземпляры


        mAutoCompleteTextView.setAdapter(
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_dropdown_item_1line, item));
        //назначаем адаптер для поля ввода, передаем ему layout автодополнени и элементы

        for (int i = 0; i < storgates.toArray().length; ++i) {
            mShopItem.add(storgates.get(i).getName()); //заполняем массив сохраненными данными
        }
        mAdapter = new StableArrayAdapter(this,R.layout.opaque_text_view, mShopItem,
                mTouchListener); //Создаем экземпляр StableArrayAdapter отвечающего
                // за обработку мапа(положения элементов)
        mListView.setAdapter(mAdapter);//назначаем адаптер
        mAutoCompleteTextView.setOnEditorActionListener(new AutoCompleteTextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) { //слушатель нажатия энтера
                if (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) { //т.е при нажатии ентер в текствью происходит то же
                    //что и при нажатии кнопки +
                    onClickAdd(v); //вызов той же функции что и для кнопки добавить
                    return true;
                }
                return false;
            }
        });
    }

    //добавление элемента
    public void onClickAdd(View view) { //событие нажатияя кнопки +
        if(mAutoCompleteTextView.getText().length()>0) { //если есть текс в текст-вью
            mShopItem.add(mAutoCompleteTextView.getText().toString()); //добавляем элемент в массив
            mAdapter.mIdMap.clear(); //чистим мап
            for (int i = 0; i < mShopItem.size(); ++i) {
                mAdapter.mIdMap.put(mShopItem.get(i), i); //заполняем мап новыми значениями
            }
            mAdapter.notifyDataSetChanged(); //уведомляем адаптер о изменении данных
            mAutoCompleteTextView.setText(""); //очищаем поле ввода
            refreshCount(); //записываем в storgate новые данные, пересчитываем количество
        }
    }

    //очистка счетчика внизу экрана
    public void refreshCount() {
        textViewCount.setText("Всего:" + mShopItem.size());
        writeFile();
    }

    //очистка всего содержимого лист-вью
    public void onDeleteAll(View view) {
        mShopItem.clear();
        mAdapter.mIdMap.clear();
        mAdapter.notifyDataSetChanged();
        refreshCount();
    }


//TODO вынести в утилиты
    void writeFile() {
        try {
            // отрываем поток для записи
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                    openFileOutput(FILENAME, MODE_PRIVATE)));
            // пишем данные
            String str="";
            for(String stringItem: mShopItem) {
                str+=stringItem+"\n";
            }
            bw.write(str);
            // закрываем поток
            bw.close();
            Log.d("Write", "Файл записан");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    } //запись в файл
    void readFile() {
        try {
            // открываем поток для чтения
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    openFileInput(FILENAME)));
            String str = "";
            // читаем содержимое
            while ((str = br.readLine()) != null) {
                mShopItem.add(str); //записываем в массив который потом пойдет в адаптер
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    } //чтение из файла

    //swipe animation
    //создаем экземпляр слушаетля для вью
    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        float mDownX;
        private int mSwipeSlop = -1;
        @Override //касание
        public boolean onTouch(final View v, MotionEvent event) {
            if (mSwipeSlop < 0) {
                mSwipeSlop = ViewConfiguration.get(MainActivity.this).
                        getScaledTouchSlop();
            }
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: //событие нажатия
                    if (mItemPressed) {
                        return false;
                    }
                    mItemPressed = true;
                    mDownX = event.getX();//координата по х
                    break;
                case MotionEvent.ACTION_CANCEL: //отпустили
                    v.setAlpha(1); //прозрачность
                    v.setTranslationX(0); //убрать смещение
                    mItemPressed = false;
                    break;
                case MotionEvent.ACTION_MOVE: //событие движения
                {
                    float x = event.getX() + v.getTranslationX(); //получаем координаты по х + смещение
                    float deltaX = x - mDownX; //смещение по х
                    float deltaXAbs = Math.abs(deltaX); //в обе стороны
                    if (!mSwiping) {
                        if (deltaXAbs > mSwipeSlop) { //если смещение больше ограничения
                            mSwiping = true;
                            mListView.requestDisallowInterceptTouchEvent(true);
                            mBackgroundContainer.showBackground(v.getTop(), v.getHeight());//рисуем подложку за движущимся элементом(отклюено)
                        }
                    }
                    if (mSwiping) {
                        v.setTranslationX((x - mDownX)); //смещение по х
                        v.setAlpha(1 - deltaXAbs / v.getWidth()); //чем дальше уводим - тем прозрачнее
                    }
                }
                break;
                case MotionEvent.ACTION_UP: //отпускаем
                {
                    // Выбор - вернуться обратно или удалить
                    if (mSwiping) {
                        float x = event.getX() + v.getTranslationX(); //получаем положение
                        float deltaX = x - mDownX; //смещение
                        float deltaXAbs = Math.abs(deltaX);
                        float fractionCovered;
                        float endX;
                        float endAlpha;
                        final boolean remove;
                        if (deltaXAbs > v.getWidth() / 4) { //если дельта больше ширины/4
                            fractionCovered = deltaXAbs / v.getWidth();
                            endX = deltaX < 0 ? -v.getWidth() : v.getWidth();//всегда положительны
                            endAlpha = 0;
                            remove = true; //удалить
                        } else {
                            // иначе вернуть обратно
                            fractionCovered = 1 - (deltaXAbs / v.getWidth());
                            endX = 0;
                            endAlpha = 1;
                            remove = false;
                        }
                        long duration = (int) ((1 - fractionCovered) * SWIPE_DURATION); //скорость
                        mListView.setEnabled(false); //отключаем лист
                        Log.v("", duration+"");
                        v.animate().setDuration(duration). //анимируем задавая скорость смещениея, конечную точку и прозрачность
                                alpha(endAlpha).translationX(endX).
                                withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        v.setAlpha(1);
                                        v.setTranslationX(0);
                                        if (remove) {
                                            animateRemoval(mListView, v); //удаляем
                                        } else {
                                            mBackgroundContainer.hideBackground(); //скрываем подложку(отключено)
                                            mSwiping = false;
                                            mListView.setEnabled(true);
                                        }
                                    }
                                });
                    }
                }
                mItemPressed = false;
                break;
                default:
                    return false;
            }
            return true;
        }
    }; //


    private void animateRemoval(final ListView listview, View viewToRemove) { //анимация удаления
        int firstVisiblePosition = listview.getFirstVisiblePosition(); //первый видимый элемент
        for (int i = 0; i < listview.getChildCount(); ++i) { //бегаем по наследникам
            View child = listview.getChildAt(i);
            if (child != viewToRemove) { //проверяем что они не помечены как удаленные
                int position = firstVisiblePosition + i; //увеличиваем позицию на i
                long itemId = mAdapter.getItemId(position); //сохраняем адрес элемента
                mItemIdTopMap.put(itemId, child.getTop()); //добавляем в мап
            }
        }
        // удаление элемента из адаптера
        int position = mListView.getPositionForView(viewToRemove);//получаем позицию удаленного
        mAdapter.remove(mAdapter.getItem(position)); //удаляем из адаптера
        refreshCount(); //обновляем данные
        final ViewTreeObserver observer = listview.getViewTreeObserver(); //дерево-обсервер
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() { //слушатель постоитель
            public boolean onPreDraw() {
                observer.removeOnPreDrawListener(this);
                boolean firstAnimation = true;
                int firstVisiblePosition = listview.getFirstVisiblePosition();
                //бегаем по наследникам, создаем анимацию перемещения
                for (int i = 0; i < listview.getChildCount(); ++i) {
                    final View child = listview.getChildAt(i);
                    int position = firstVisiblePosition + i;
                    long itemId = mAdapter.getItemId(position);
                    Integer startTop = mItemIdTopMap.get(itemId);
                    int top = child.getTop();
                    if (startTop != null) {
                        if (startTop != top) {
                            int delta = startTop - top;
                            child.setTranslationY(delta);
                            child.animate().setDuration(MOVE_DURATION).translationY(0);
                            if (firstAnimation) {
                                child.animate().withEndAction(new Runnable() {
                                    public void run() {
                                        mBackgroundContainer.hideBackground();
                                        mSwiping = false;
                                        mListView.setEnabled(true);
                                    }
                                });
                                firstAnimation = false;
                            }
                        }
                    } else {
                        int childHeight = child.getHeight() + listview.getDividerHeight();
                        startTop = top + (i > 0 ? childHeight : -childHeight);
                        int delta = startTop - top;
                        child.setTranslationY(delta);
                        child.animate().setDuration(MOVE_DURATION).translationY(0);
                        if (firstAnimation) {
                            child.animate().withEndAction(new Runnable() {
                                public void run() {
                                    mBackgroundContainer.hideBackground();
                                    mSwiping = false;
                                    mListView.setEnabled(true);
                                }
                            });
                            firstAnimation = false;
                        }
                    }
                }
                mItemIdTopMap.clear();
                return true;
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_lists: {this.finish();} break;
        }
        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }
}
